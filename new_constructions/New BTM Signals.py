# Databricks notebook source
# MAGIC %md
# MAGIC # Analysis of New BTM, NFC, Offline Signals
# MAGIC
# MAGIC Since this is a new signal, I will be starting analysis from scratch. Therefore, this notebook will contain all my findings and dicsoveries relating to this signal

# COMMAND ----------

# MAGIC %md
# MAGIC ## Defining new signals
# MAGIC
# MAGIC New singals have been provided by Vivek Ravi as of 12/17, and we'll first define them to ensure that we can access them normally.

# COMMAND ----------

NEW_SIGNALS_BTM = [
  "RTE_BTM_LFD_LOSS_COMM_DTC",
  "RTE_BTM_RFD_LOSS_COMM_DTC",
  "RTE_BTM_IC_LOSS_COMM_DTC",
  "RTE_BTM_FF_LOSS_COMM_DTC",
  "RTE_BTM_RF_LOSS_COMM_DTC",
]

NEW_SIGNALS_NFC = [
  "RTE_NFC_ID_LOSS_COMM_DTC",
  "RTE_NFC_ID_LOSS_COMM_DTC"
]

# COMMAND ----------

# MAGIC %md
# MAGIC ## Defining access tables
# MAGIC
# MAGIC From initial analysis, it looks like `main.telemetry.vehicle_customer` is where these signals might exist / be located. Let's define, and try to read these signals to get some kind of a picture, regarding what these signals appear to be like

# COMMAND ----------

MEDFI = "main.telemetry.vehicle_customer"

# COMMAND ----------

# MAGIC %md
# MAGIC Setting up a PySpark read environment

# COMMAND ----------

from pyspark.sql import SparkSession
from pyspark.sql.functions import col, current_date

# Create a Spark session
spark = SparkSession.builder.appName("SignalAnalysis").getOrCreate()

# COMMAND ----------

df = spark.read.table(MEDFI)

# COMMAND ----------

# Combine the signal lists
NEW_SIGNALS = NEW_SIGNALS_BTM + NEW_SIGNALS_NFC

# COMMAND ----------

filtered_df = df.filter(
    (col("signal_name").isin(NEW_SIGNALS)) & 
    (col("date") == current_date())
)
final_df = filtered_df.select()
final_df.show()

# COMMAND ----------

selected_columns = [
    "date", "id", "timestamp", "signal_name", 
    "signal_long_name", "signal_value"
]
final_df = filtered_df.select(*selected_columns)
final_df.show()

# COMMAND ----------

# MAGIC %md
# MAGIC This is suspicious - somehow, there are no rows that are picked up, when we search for these signal names. This MAY be because for the current day, so to confirm the non presence of signals, let us run for a different set of days, perhaps over a week

# COMMAND ----------

from pyspark.sql.functions import lit
from pyspark.sql.types import DateType

# COMMAND ----------

# Define the date range
start_date = lit("2023-12-01").cast(DateType())
end_date = lit("2023-12-07").cast(DateType())

# COMMAND ----------

filtered_df = df.filter(
    (col("signal_name").isin(NEW_SIGNALS)) &
    (col("date") >= start_date) & 
    (col("date") <= end_date)
)
final_df = filtered_df.select(*selected_columns)
final_df.show()

# COMMAND ----------


