# Databricks notebook source
import pandas as pd
from pyspark.sql import SparkSession
import plotly.express as px
from matplotlib import pyplot as plt
from pyspark.sql import functions as F

# COMMAND ----------

spark_session = SparkSession.builder.appName("spark").getOrCreate()

# COMMAND ----------

table_one = "main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames"
table_two = "main.adhoc.ss_t_btm_nfc_offline_daily_temp_and_can_frames"

# COMMAND ----------

def run_query(table_name):
  query = f"""
    SELECT * FROM {table_name}
    WHERE event_date BETWEEN '2023-06-01' AND '2023-06-02'
  """
  return spark.sql(query)


# COMMAND ----------

df_original = run_query(table_one)
df_new = run_query(table_two)

# COMMAND ----------

df_new = df_new.dropDuplicates()
df_new.write.mode("overwrite").saveAsTable(table_two)

# COMMAND ----------

df_original.head()
df_new.head()

# COMMAND ----------

def create_graphs(df):
  columns = [
    "BTM_FF_offline_temp",
    "BTM_LFD_offline_temp",
    "BTM_IC_offline_temp",
    "BTM_RF_offline_temp"
  ]
  # Group by 'event_date' and sum the specified columns
  grouped_df = df.groupby('event_date')[columns].sum()

  # Plotting each column as a separate timeline graph
  for column in columns:
      plt.figure(figsize=(10, 4))
      plt.plot(grouped_df.index, grouped_df[column], marker='o')
      plt.title(f"Sum of {column} over Time")
      plt.xlabel("Date")
      plt.ylabel("Sum")
      plt.grid(True)
      plt.show()


# COMMAND ----------

def create_graphs_pyspark(spark_df):
    # Ensure event_date is a DateType
    spark_df = spark_df.withColumn("event_date", F.to_date("event_date"))
    spark_df = spark_df.dropDuplicates()
    # Define the columns to sum
    columns = [
        "BTM_FF_offline_temp",
        "BTM_LFD_offline_temp",
        "BTM_IC_offline_temp",
        "BTM_RF_offline_temp"
    ]

    # Group by 'event_date' and sum the columns
    grouped_df = spark_df.groupBy("event_date").agg(*[F.sum(col).alias(col) for col in columns])

    # Convert to Pandas DataFrame for plotting
    pandas_df = grouped_df.toPandas()
    pandas_df.set_index("event_date", inplace=True)
    
    # Plotting each column
    for column in columns:
        plt.figure(figsize=(10, 4))
        plt.scatter(pandas_df.index, pandas_df[column])
        plt.title(f"Sum of {column} over Time")
        plt.xlabel("Date")
        plt.ylabel("Sum")
        plt.grid(True)
        plt.show()

# COMMAND ----------

create_graphs_pyspark(df_new)

# COMMAND ----------

create_graphs_pyspark(df_original)

# COMMAND ----------

def plot_vin_errors(spark_df, top_n=10):
    # Define the columns that indicate errors
    error_columns = [
        "RFD_offline_flag", "LFD_offline_flag", "RF_offline_flag", 
        "FF_offline_flag", "IC_offline_flag", 
        "BTM_RFD_offline_temp", "BTM_LFD_offline_temp", "BTM_RF_offline_temp", 
        "BTM_FF_offline_temp", "BTM_IC_offline_temp"
    ]

    # Aggregate errors for each VIN
    # Sum each column individually where the value is greater than 0
    agg_df = spark_df.groupBy("vin").agg(
        *[F.sum(F.when(F.col(col) > 0, 1).otherwise(0)).alias(col) for col in error_columns]
    )

    # Calculate total errors per VIN by summing up all the individual error counts
    total_error_expr = sum([F.col(col) for col in error_columns])
    agg_df = agg_df.withColumn("total_errors", total_error_expr)

    # Select VIN and total_errors, sort by total_errors in descending order, and limit to top N
    agg_df = agg_df.select("vin", "total_errors").orderBy(F.desc("total_errors")).limit(top_n)

    # Convert to Pandas DataFrame for plotting
    pandas_df = agg_df.toPandas()

    # Plotting
    pandas_df.plot(kind='bar', x='vin', y='total_errors', figsize=(15, 6), title='Top VINs with Most Errors')
    plt.xlabel("VIN")
    plt.ylabel("Total Errors")
    plt.show()

# COMMAND ----------

plot_vin_errors(df_new, 50)

# COMMAND ----------

plot_vin_errors(df_original, 50)

# COMMAND ----------

def get_top_vins(spark_df, top_n=50):
    # Define the columns that indicate errors
    error_columns = [
        "RFD_offline_flag", "LFD_offline_flag", "RF_offline_flag", 
        "FF_offline_flag", "IC_offline_flag", 
        "BTM_RFD_offline_temp", "BTM_LFD_offline_temp", "BTM_RF_offline_temp", 
        "BTM_FF_offline_temp", "BTM_IC_offline_temp"
    ]

    # Aggregate errors for each VIN
    agg_df = spark_df.groupBy("vin").agg(
        *[F.sum(F.when(F.col(col) > 0, 1).otherwise(0)).alias(col) for col in error_columns]
    )

    # Calculate total errors per VIN
    total_error_expr = sum([F.col(col) for col in error_columns])
    agg_df = agg_df.withColumn("total_errors", total_error_expr)

    # Select VIN and total_errors, sort by total_errors in descending order, and limit to top N
    return agg_df.select("vin", "total_errors").orderBy(F.desc("total_errors")).limit(top_n)

def find_common_top_vins(df1, df2, top_n=50):
    # Get top VINs for each DataFrame
    top_vins_df1 = get_top_vins(df1, top_n)
    top_vins_df2 = get_top_vins(df2, top_n)

    # Find common VINs
    common_vins = top_vins_df1.join(top_vins_df2, top_vins_df1.vin == top_vins_df2.vin).select(top_vins_df1.vin)

    return common_vins

# COMMAND ----------

vins = find_common_top_vins(df_original, df_new, 100).toPandas()

# COMMAND ----------

print(vins.head())

# COMMAND ----------

def plot_vin_errors_BTM_FF_offline_temp(spark_df, top_n=10):
    # Define the columns that indicate errors
    error_columns = [
        "BTM_FF_offline_temp"
    ]

    # Aggregate errors for each VIN
    # Sum each column individually where the value is greater than 0
    agg_df = spark_df.groupBy("vin").agg(
        *[F.sum(F.when(F.col(col) > 0, 1).otherwise(0)).alias(col) for col in error_columns]
    )

    # Calculate total errors per VIN by summing up all the individual error counts
    total_error_expr = sum([F.col(col) for col in error_columns])
    agg_df = agg_df.withColumn("BTM_FF_offline_temp", total_error_expr)

    # Select VIN and total_errors, sort by total_errors in descending order, and limit to top N
    agg_df = agg_df.select("vin", "BTM_FF_offline_temp").orderBy(F.desc("BTM_FF_offline_temp")).limit(top_n)

    # Convert to Pandas DataFrame for plotting
    pandas_df = agg_df.toPandas()

    # Plotting
    pandas_df.plot(kind='bar', x='vin', y='BTM_FF_offline_temp', figsize=(15, 6), title='Top VINs with Most Errors')
    plt.xlabel("VIN")
    plt.ylabel("Total Errors")
    plt.show()

# COMMAND ----------

plot_vin_errors_BTM_FF_offline_temp(df_original, 50)

# COMMAND ----------

plot_vin_errors_BTM_FF_offline_temp(df_new, 50)

# COMMAND ----------


