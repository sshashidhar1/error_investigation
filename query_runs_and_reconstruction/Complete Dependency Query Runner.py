# Databricks notebook source
# MAGIC %md
# MAGIC # BTM Error Reproduction

# COMMAND ----------

# module imports
import json
import pandas as pd
from typing import Tuple
from datetime import datetime, timedelta
from pyspark.sql import SparkSession
from pyspark.sql.functions import max as spark_max

# file imports
from query_engine import QueryEngine

# COMMAND ----------

# load the queries
with open("./query_list.json", "r") as f:
  queries = json.load(f)

# COMMAND ----------

spark_session = SparkSession.builder.appName("spark").getOrCreate()

# COMMAND ----------

def pull_new_data(table_name: str, spark: SparkSession) -> Tuple[str, str]:
    """
    For a given Databricks SQL table, looks up the last date in the table, and returns
    a range from the last date to the current day minus one, using PySpark.

    Args:
    table_name (str): Name of the table to query.
    spark (SparkSession): A SparkSession object for database interaction.

    Returns:
    Tuple[str, str]: A tuple containing the start date and end date for the new data pull.

    Raises:
    ValueError: If the table is empty or does not exist.
    """
    try:
        # Load the table
        df = spark.table(table_name)

        # Query to find the last date in the table
        last_date_df = df.select(spark_max("event_date")).collect()
        last_date = last_date_df[0][0]

        # Ensure the last date is valid
        if last_date is None:
            raise ValueError("The last date in the table is None.")
        
        last_date + timedelta(days = 1)
        start_date = last_date.strftime('%Y-%m-%d')
        end_date = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')

        return start_date, end_date

    except Exception as e:
        # Handle any errors that occur during the database query
        raise ValueError(f"An error occurred while querying the table: {e}")

# COMMAND ----------

def pull_new_data_btm(table_name: str, spark: SparkSession) -> Tuple[str, str]:
    """
    For a given Databricks SQL table, looks up the last date in the table, and returns
    a range from the last date to the current day minus one, using PySpark.

    Args:
    table_name (str): Name of the table to query.
    spark (SparkSession): A SparkSession object for database interaction.

    Returns:
    Tuple[str, str]: A tuple containing the start date and end date for the new data pull.

    Raises:
    ValueError: If the table is empty or does not exist.
    """
    try:
        # Load the table
        df = spark.table(table_name)

        # Query to find the last date in the table
        last_date_df = df.select(spark_max("date")).collect()
        last_date = last_date_df[0][0]

        # Ensure the last date is valid
        if last_date is None:
            raise ValueError("The last date in the table is None.")
        
        last_date + timedelta(days = 1)
        start_date = last_date.strftime('%Y-%m-%d')
        end_date = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')

        return start_date, end_date

    except Exception as e:
        # Handle any errors that occur during the database query
        raise ValueError(f"An error occurred while querying the table: {e}")

# COMMAND ----------

def staterun(q):
  q.resegment_date_ranges()
  q.execute_query()
  del q
  return

# COMMAND ----------

# MAGIC %md
# MAGIC # Power Mode Tables

# COMMAND ----------

table = "main.adhoc.nb_power_modes_events"
dates = [pull_new_data(table, spark_session)]
query = queries[table]

stateful = QueryEngine(
  table = table,
  dates = dates,
  query = query
)
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_power_modes_events_duration_ALL'
dates = [pull_new_data(table, spark_session)]
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query)
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_power_cycles_duration'
dates = None
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query, method = 'overwrite')
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_power_cycles_duration'
dates = None
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query, method = 'overwrite')
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_power_cycles_duration_with_power_state'
dates = None
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query, method = 'overwrite')
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_power_cycles_duration_with_power_state_vin'
dates = None
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query, method = 'overwrite')
staterun(stateful)

# COMMAND ----------

# MAGIC %md
# MAGIC # BTM Runners

# COMMAND ----------

table = 'main.adhoc.nb_btm_nfc_sensor_health'
dates = [pull_new_data_btm(table, spark_session)]
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query)
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_btm_nfc_sensor_health_temp'
dates = [pull_new_data_btm(table, spark_session)]
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query)
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_btm_nfc_offline_with_vin_power_cycles'
dates = [pull_new_data(table, spark_session)]
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query)
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_btm_nfc_offline_daily_with_vin'
dates = [pull_new_data(table, spark_session)]
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query)
staterun(stateful)

# COMMAND ----------

# MAGIC %md
# MAGIC # CAN Population

# COMMAND ----------

table = 'main.adhoc.nb_BTM_can_errors_log_count'
dates = [pull_new_data_btm(table, spark_session)]
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query)
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_BTM_can_errors_with_vin_power_cycles'
dates = [pull_new_data(table, spark_session)]
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query)
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_BTM_can_errors_daily_with_vin'
dates = [pull_new_data(table, spark_session)]
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query)
staterun(stateful)

# COMMAND ----------

# MAGIC %md
# MAGIC # CAN Frames

# COMMAND ----------

table = 'main.adhoc.nb_btm_nfc_sensor_CAN_frames_lowfi'
dates = [pull_new_data_btm(table, spark_session)]
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query)
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_btm_nfc_CAN_frames_lowfi_flag'
dates = [("2023-11-24", "2023-11-30")]
dates = [("2023-01-01", (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d'))]
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query, method = 'overwrite')
staterun(stateful)

# COMMAND ----------

table = 'main.adhoc.nb_btm_nfc_health_power_cycles_CAN_frames'
dates = [pull_new_data(table, spark_session)]
query = queries[table]
stateful = QueryEngine(table = table, dates = dates, query = query)
staterun(stateful)

# COMMAND ----------


