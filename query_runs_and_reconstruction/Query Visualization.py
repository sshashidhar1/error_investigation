# Databricks notebook source
!pip install pydot
!pip install pydot-ng
!pip install matplotlib
!pip install networkx

# COMMAND ----------

import networkx as nx
import matplotlib.pyplot as plt

# COMMAND ----------

def create_dependency_graph(dependency_dict: dict) -> nx.DiGraph:
    """
    Create a directed graph from a dictionary of dependencies.

    Args:
    dependency_dict (dict): A dictionary representing the dependencies where
                            keys are tables and values are lists of tables they depend on.

    Returns:
    nx.DiGraph: A directed graph representing the table dependencies.
    """
    graph = nx.DiGraph()
    for table, dependencies in dependency_dict.items():
        for dependency in dependencies:
            graph.add_edge(dependency, table)
    return graph

# COMMAND ----------

def visualize_graph(graph: nx.DiGraph):
    """
    Visualize the directed graph using matplotlib.

    Args:
    graph (nx.DiGraph): The directed graph to visualize.
    """
    nx.draw(graph, with_labels=True)
    plt.show()

# COMMAND ----------

data = {
    "main.adhoc.nb_power_modes_events": ["low_fidelity_data.denormalized_obj"],
    "main.adhoc.nb_power_modes_events_duration" : ["main.adhoc.nb_power_modes_events"],
    "main.adhoc.nb_power_modes_events_duration_ALL": ["main.adhoc.nb_power_modes_events_duration"],
    "main.adhoc.nb_power_cycles_duration": ["main.adhoc.nb_power_modes_events_duration_ALL"],
    "main.adhoc.nb_power_cycles_duration_ALL": ["main.adhoc.nb_power_cycles_duration", "main.adhoc.nb_power_modes_events_duration_ALL"],
    "main.adhoc.nb_power_cycles_duration_with_power_state" : ["main.adhoc.nb_power_cycles_duration_ALL", "main.adhoc.nb_power_modes_events_duration_ALL"],
    "main.adhoc.nb_power_cycles_duration_with_power_state_vin" : ["main.adhoc.nb_power_cycles_duration_with_power_state", "main.rel_fleet_metrics.vehicles_metadata"],

    "main.adhoc.nb_btm_nfc_sensor_health" : ["low_fidelity_data.denormalized_obj"],
    "main.adhoc.nb_btm_nfc_sensor_health_temp" : ["main.adhoc.nb_btm_nfc_sensor_health"],
    "main.adhoc.nb_btm_nfc_sensor_health_with_vin_power_cycles" : ["main.adhoc.nb_power_cycles_duration_with_power_state_vin", "main.adhoc.nb_btm_nfc_sensor_health_temp"],
    "main.adhoc.nb_btm_nfc_offline_with_vin_power_cycles" : ["main.adhoc.nb_btm_nfc_sensor_health_with_vin_power_cycles"],
    "main.adhoc.nb_btm_nfc_offline_daily_with_vin" : ["main.adhoc.nb_btm_nfc_sensor_health_with_vin_power_cycles"],
    "main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames": ["main.adhoc.nb_btm_nfc_offline_can_frames", "main.adhoc.nb_btm_nfc_offline_daily_with_vin"],

    "main.adhoc.nb_BTM_can_errors_log_count": ["high_fidelity_customer_vehicles_data.parsed_pcap_message_customer_vehicles_v2"],
    "main.adhoc.nb_BTM_can_errors_with_vin_power_cycles" : ["main.adhoc.nb_power_cycles_duration_with_power_state_vin", "main.adhoc.nb_BTM_can_errors_log_count"],
    "main.adhoc.nb_BTM_can_errors_daily_with_vin" : ["main.adhoc.nb_BTM_can_errors_with_vin_power_cycles"],
    "main.adhoc.nb_BTM_can_errors_daily_module_status_transition" : ["main.adhoc.nb_btm_nfc_offline_CAN_frames_with_errors_daily"],

    "main.adhoc.nb_btm_nfc_sensor_CAN_frames_lowfi" : ["low_fidelity_data.denormalized_obj"],
    "main.adhoc.nb_btm_nfc_CAN_frames_lowfi_flag" : ["main.adhoc.nb_btm_nfc_sensor_CAN_frames_lowfi"],
    "main.adhoc.nb_btm_nfc_health_power_cycles_CAN_frames": ["main.adhoc.nb_power_cycles_duration_with_power_state_vin", "main.adhoc.nb_btm_nfc_CAN_frames_lowfi_flag"],
    "main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames_SECOND" : ["main.adhoc.nb_btm_nfc_health_power_cycles_CAN_frames", "main.adhoc.nb_btm_nfc_offline_daily_with_vin"],
    "main.adhoc.nb_btm_nfc_offline_CAN_frames_with_errors_daily": ["main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames", "main.adhoc.nb_BTM_can_errors_daily_with_vin"]
}


# COMMAND ----------

test_graph = create_dependency_graph(data)

# COMMAND ----------

visualize_graph(test_graph)

# COMMAND ----------

 # Function to create a directed graph from the provided dictionary
def create_dependency_graph(dependency_dict: dict) -> nx.DiGraph:
    """
    Create a directed graph from a dictionary of dependencies.

    Args:
    dependency_dict (dict): A dictionary representing the dependencies where
                            keys are tables and values are lists of tables they depend on.

    Returns:
    nx.DiGraph: A directed graph representing the table dependencies.
    """
    graph = nx.DiGraph()
    for table, dependencies in dependency_dict.items():
        for dependency in dependencies:
            graph.add_edge(dependency, table)
    return graph

# Function to visualize the graph using spring layout
def visualize_graph(graph: nx.DiGraph):
    """
    Visualize the directed graph using a spring layout.

    Args:
    graph (nx.DiGraph): The directed graph to visualize.
    """
    # Use the spring layout for positioning
    pos = nx.spring_layout(graph, k=0.5, iterations=20)
    
    # Set the size of the plot
    plt.figure(figsize=(20, 15))
    
    # Draw the nodes, edges, and labels
    nx.draw_networkx_nodes(graph, pos, node_size=7000, node_color='skyblue', alpha=0.9)
    nx.draw_networkx_edges(graph, pos, arrowstyle='->', arrowsize=20)
    nx.draw_networkx_labels(graph, pos, font_size=10)
    
    # Adjust margins and turn off the axis
    plt.margins(0.1)
    plt.axis('off')
    
    # Display the graph
    plt.show()

# COMMAND ----------

# Create the graph from the dependency dictionary
graph = create_dependency_graph(data)

# Visualize the graph
visualize_graph(graph)

# COMMAND ----------

from collections import defaultdict

# Function to create a directed graph from the provided dictionary
def create_dependency_graph(dependency_dict: dict) -> nx.DiGraph:
    """
    Create a directed graph from a dictionary of dependencies.

    Args:
    dependency_dict (dict): A dictionary representing the dependencies where
                            keys are tables and values are lists of tables they depend on.

    Returns:
    nx.DiGraph: A directed graph representing the table dependencies.
    """
    graph = nx.DiGraph()
    for table, dependencies in dependency_dict.items():
        for dependency in dependencies:
            graph.add_edge(dependency, table)
    return graph

def create_layered_tree_layout(graph: nx.DiGraph):
    """
    Create a layered tree layout for a directed graph.

    Args:
    graph (nx.DiGraph): The directed graph for which to create the layout.

    Returns:
    dict: A dictionary of positions keyed by node.
    """
    # Perform a topological sort to order nodes by hierarchy
    sorted_nodes = list(nx.topological_sort(graph))
    layers = defaultdict(list)
    
    # Calculate the max width for horizontal spacing
    max_width = len(sorted_nodes)
    
    # Determine the hierarchy level (layer) for each node
    for node in sorted_nodes:
        # The layer of the node is determined by the maximum layer of its predecessors plus one
        # Root nodes will default to layer 0
        layer = 0 if not graph.pred[node] else max(layers[pred] for pred in graph.pred[node]) + 1
        layers[node] = layer
    
    # Group nodes by layer
    layers_by_level = defaultdict(list)
    for node, level in layers.items():
        layers_by_level[level].append(node)
    
    # Assign positions to nodes within each layer
    pos = {}
    for level, nodes in layers_by_level.items():
        width = max_width / (len(nodes) + 1)
        for i, node in enumerate(sorted(nodes)):
            pos[node] = ((i + 1) * width, -level)

    return pos

# Placeholder for the full graph data dictionary
dependency_dict = {
    # ... (your full graph data) ...
}

# Create the graph from the dependency dictionary
graph = create_dependency_graph(data)

# Create the layered tree layout for the graph
pos = create_layered_tree_layout(graph)

# Set the size of the plot
plt.figure(figsize=(20, 15))

# Draw the nodes, edges, and labels
nx.draw_networkx_nodes(graph, pos, node_size=7000, node_color='lightblue', alpha=0.9)
nx.draw_networkx_edges(graph, pos, arrowstyle='->', arrowsize=20)
nx.draw_networkx_labels(graph, pos, font_size=10)

# Adjust margins and turn off the axis
plt.margins(0.1)
plt.axis('off')

# Display the graph
plt.show()

# COMMAND ----------


