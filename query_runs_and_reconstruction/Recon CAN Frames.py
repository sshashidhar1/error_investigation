# Databricks notebook source
# module imports
import json
import pandas as pd
from typing import Tuple
from datetime import datetime, timedelta
from pyspark.sql import SparkSession
from pyspark.sql.functions import max as spark_max

# COMMAND ----------

spark_session = SparkSession.builder.appName("spark").getOrCreate()

# COMMAND ----------

table_to_recreate = 'main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames'
new_table = 'main.adhoc.ss_t_btm_nfc_offline_daily_temp_and_can_frames'

# COMMAND ----------

date_col = "event_date"
min_date = "2023-06-01"
max_date = "2023-06-02"

# COMMAND ----------

extract_q = f"""
SELECT * FROM {table_to_recreate} WHERE event_date BETWEEN '{min_date}' AND '{max_date}'
"""
new_q = f"""
SELECT * FROM {new_table} WHERE event_date BETWEEN '{min_date}' AND '{max_date}'
"""

# COMMAND ----------

df = spark_session.sql(extract_q)
df_2 = spark_session.sql(new_q)

# COMMAND ----------

truth = df.toPandas()
new = df_2.toPandas()

# COMMAND ----------

truth = truth.sort_values(by = "event_date")
reconstructed = new.sort_values(by = "event_date")

# COMMAND ----------


import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

def calculate_statistics(df: pd.DataFrame) -> pd.DataFrame:
    """
    Calculates various general statistics for the given DataFrame.

    Args:
    df (pd.DataFrame): The DataFrame containing vehicle data.

    Returns:
    pd.DataFrame: A DataFrame containing the calculated statistics.
    """
    stats = {
        "Total Rows": [len(df)],
        "Distinct VINs": [df['vin'].nunique()],
        "Average Duration Active (sec)": [df['duration_active_sec'].mean()],
        "Highest Power State Mean": [df['highest_power_state'].mean()],
        "BTM_RFD_offline_temp": [df['BTM_RFD_offline_temp'].mean()],
        "BTM_LFD_offline_temp": [df['BTM_LFD_offline_temp'].mean()],
        "BTM_RF_offline_temp" : [df['BTM_RF_offline_temp'].mean()],
        "BTM_FF_offline_temp" : [df['BTM_FF_offline_temp'].mean()],
        "BTM_IC_offline_temp" : [df['BTM_IC_offline_temp'].mean()],
        "RFD_offline_flag": [df['BTM_RFD_offline_temp'].mean()],
        "LFD_offline_flag" : [df['LFD_offline_flag'].mean()],
        "RF_offline_flag" : [df['RF_offline_flag'].mean()],
    }
    return pd.DataFrame(stats)

def compare_dataframes(df1: pd.DataFrame, df2: pd.DataFrame) -> None:
    """
    Compares two DataFrames and creates visualizations to show differences.

    Args:
    df1 (pd.DataFrame): The first DataFrame to compare.
    df2 (pd.DataFrame): The second DataFrame to compare.
    """
    # Calculate statistics for both dataframes
    stats_df1 = calculate_statistics(df1)
    stats_df2 = calculate_statistics(df2)

    # Merging the statistics for comparison
    merged_stats = pd.concat([stats_df1, stats_df2], axis=0).reset_index(drop=True)
    
    for column in merged_stats.columns:
        fig = go.Figure(data=[
            go.Bar(name='DataFrame 1', x=[column], y=[merged_stats.loc[0, column]]),
            go.Bar(name='DataFrame 2', x=[column], y=[merged_stats.loc[1, column]])
        ])

        # Customize aspects of the plot
        fig.update_layout(title=f'Comparison of {column}', barmode='group')
        fig.show()



    return merged_stats

# COMMAND ----------

compare_dataframes(truth, reconstructed)

# COMMAND ----------

for col in ['RFD_offline_flag', 'LFD_offline_flag', 'RF_offline_flag', 'FF_offline_flag', 'IC_offline_flag', 'BTM_RFD_offline_temp', 'BTM_LFD_offline_temp', 'BTM_RF_offline_temp', 'BTM_FF_offline_temp', 'BTM_IC_offline_temp', 'NFC_FD_offline', 'NFC_ID_offline']:
  summed_data = reconstructed.groupby('event_date')[col].sum().reset_index()
  fig = px.bar(summed_data, x='event_date', y=col, title=f'Sum of {col} per Day')
  fig.show()  

# COMMAND ----------

for col in ['RFD_offline_flag', 'LFD_offline_flag', 'RF_offline_flag', 'FF_offline_flag', 'IC_offline_flag', 'BTM_RFD_offline_temp', 'BTM_LFD_offline_temp', 'BTM_RF_offline_temp', 'BTM_FF_offline_temp', 'BTM_IC_offline_temp', 'NFC_FD_offline', 'NFC_ID_offline']:
  summed_data = reconstructed.groupby('event_date')[col].sum().reset_index()
  fig = px.line(summed_data, x='event_date', y=col, title=f'Sum of {col} per Day')
  fig.show()  

# COMMAND ----------


