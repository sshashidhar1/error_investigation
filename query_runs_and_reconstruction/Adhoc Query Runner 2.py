# Databricks notebook source
import json
# file imports
from query_engine import QueryEngine

# COMMAND ----------

# load the queries
with open("./query_list.json", "r") as f:
  queries = json.load(f)

# COMMAND ----------

table = "main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames"
dates = [("2023-10-20", "2023-10-25")]
method = "append"
query = """

SELECT a.vin,
       a.car_ownership_id,
       a._source,
       a.event_date,
       sum(a.cycle_duration_sec) AS duration_active_sec,
       max(a.power_cycle_number) AS total_power_cycles,
       max(a.highest_power_state_num) AS highest_power_state,
       sum(RFD_CAN_frames_count) AS RFD_CAN_frames_count_daily,
       sum(LFD_CAN_frames_count) AS LFD_CAN_frames_count_daily,
       sum(RF_CAN_frames_count) AS RF_CAN_frames_count_daily,
       sum(FF_CAN_frames_count) AS FF_CAN_frames_count_daily,
       sum(IC_CAN_frames_count) AS IC_CAN_frames_count_daily,
       if(RFD_CAN_frames_count_daily>0, 0, 1) AS RFD_offline_flag,
       if(LFD_CAN_frames_count_daily>0, 0, 1) AS LFD_offline_flag,
       if(RF_CAN_frames_count_daily>0, 0, 1) AS RF_offline_flag,
       if(FF_CAN_frames_count_daily>0, 0, 1) AS FF_offline_flag,
       if(IC_CAN_frames_count_daily>0, 0, 1) AS IC_offline_flag,
       max(BTM_RFD_offline) AS BTM_RFD_offline_temp,
       max(BTM_LFD_offline) AS BTM_LFD_offline_temp,
       max(BTM_RF_offline) AS BTM_RF_offline_temp,
       max(BTM_FF_offline) AS BTM_FF_offline_temp,
       max(BTM_IC_offline) AS BTM_IC_offline_temp,
       max(NFC_FD_offline) AS NFC_FD_offline,
       max(NFC_ID_offline) AS NFC_ID_offline
FROM main.adhoc.nb_btm_nfc_health_power_cycles_CAN_frames AS a
LEFT JOIN main.adhoc.nb_btm_nfc_offline_daily_with_vin AS b ON a.vin = b.vin
AND a.event_date = b.event_date
WHERE a.event_date >= '{min_date}'
  AND a.event_date <= '{max_date}'
GROUP BY a.vin,
         a.car_ownership_id,
         a._source,
         a.event_date
ORDER BY a._source,
         a.event_date

"""

# COMMAND ----------

x = QueryEngine(
  table = "main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames",
  dates = dates,
  method = method,
  query = query
)

# COMMAND ----------

x.resegment_date_ranges()

# COMMAND ----------

x.execute_query()

# COMMAND ----------

y = QueryEngine(
  table = "main.adhoc.ss_btm_nfc_offline_daily_temp_and_can_frames",
  dates = ("2023-10-20", "2023-11-08"),
  method = "overwrite",
  query = "SELECT * FROM main.adhoc.ss_btm_nfc_offline_daily_temp_and_can_frames WHERE event_date >= '{min_date}' AND event_date <= '{max_date}'"
)

# COMMAND ----------



# COMMAND ----------

y.execute_query()

# COMMAND ----------


