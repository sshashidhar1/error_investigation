# Databricks notebook source
# MAGIC %md
# MAGIC # Resilent Runner

# COMMAND ----------

# module imports
import json
import pandas as pd
from typing import Tuple
from datetime import datetime, timedelta
from pyspark.sql import SparkSession
from pyspark.sql.functions import max as spark_max

# file imports
from new_query_engine import QueryEngine

# COMMAND ----------

import time

# COMMAND ----------

def replace_nb_with_ssx(input_string):
    # Replace all occurrences of 'nb' with 'ssy'
    return input_string.replace("nb", "ss_f")

# COMMAND ----------

spark = SparkSession.builder.appName("Dedupe").getOrCreate()

# COMMAND ----------

def deduplicate_and_write_table(table_name):
    # Initialize Spark session
    global spark

    # Read the table
    df = spark.table(table_name)

    # Drop duplicate rows
    df_deduplicated = df.dropDuplicates()

    # Overwrite the original table with the deduplicated DataFrame
    df_deduplicated.write.format("delta").mode("overwrite").saveAsTable(table_name)

# COMMAND ----------

# load the queries
with open("./query_list.json", "r") as f:
  queries = json.load(f)

# COMMAND ----------

def staterun(q):
  q.resegment_date_ranges()
  q.execute_query()
  del q
  return

# COMMAND ----------

spark_session = SparkSession.builder.appName("spark").getOrCreate()

# COMMAND ----------

dates_ranges = [("2023-06-01", "2023-12-19")]

# COMMAND ----------

TABLE_RANGE = [
  # "main.adhoc.nb_power_modes_events",
  "main.adhoc.nb_power_modes_events_duration",
  "main.adhoc.nb_power_modes_events_duration_ALL",
  "main.adhoc.nb_power_cycles_duration",
  "main.adhoc.nb_power_cycles_duration_ALL",
  "main.adhoc.nb_power_cycles_duration_with_power_state",
  "main.adhoc.nb_power_cycles_duration_with_power_state_vin",
  "main.adhoc.nb_btm_nfc_sensor_health",
  "main.adhoc.nb_btm_nfc_sensor_health_temp",
  "main.adhoc.nb_btm_nfc_sensor_health_with_vin_power_cycles",
  "main.adhoc.nb_btm_nfc_offline_with_vin_power_cycles",
  "main.adhoc.nb_btm_nfc_offline_daily_with_vin",
  "main.adhoc.nb_BTM_can_errors_log_count",
  "main.adhoc.nb_BTM_can_errors_with_vin_power_cycles",
  "main.adhoc.nb_BTM_can_errors_daily_with_vin",
  "main.adhoc.nb_btm_nfc_sensor_CAN_frames_lowfi",
  "main.adhoc.nb_btm_nfc_CAN_frames_lowfi_flag",
  "main.adhoc.nb_btm_nfc_health_power_cycles_CAN_frames",
  "main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames"
]

# COMMAND ----------

for table in TABLE_RANGE:
  start = time.time()
  print(f"Table: {table}")
  query = replace_nb_with_ssx(queries[table])
  table = replace_nb_with_ssx(table)
  stateful = QueryEngine(
    table = table,
    dates = dates_ranges,
    query = query,
    segment_length=30
  )
  staterun(stateful)
  end = time.time()
  deduplicate_and_write_table(table)
  print(f"Table: {table}. Total Time {end - start}")


# COMMAND ----------


