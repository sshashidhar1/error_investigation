# Databricks notebook source
!pip install networkx
!pip install graphviz
!pip install pydot
!pip install pydot-ng
!pip install pygraphviz

# COMMAND ----------

import networkx as nx
import matplotlib.pyplot as plt

# COMMAND ----------

# Dependency graph data
dependency_graph = {
    "main.adhoc.nb_power_modes_events": ["low_fidelity_data.denormalized_obj"],
    "main.adhoc.nb_power_modes_events_duration" : ["main.adhoc.nb_power_modes_events"],
    "main.adhoc.nb_power_modes_events_duration_ALL": ["main.adhoc.nb_power_modes_events_duration"],
    "main.adhoc.nb_power_cycles_duration": ["main.adhoc.nb_power_modes_events_duration_ALL"],
    "main.adhoc.nb_power_cycles_duration_ALL": ["main.adhoc.nb_power_cycles_duration", "main.adhoc.nb_power_modes_events_duration_ALL"],
    "main.adhoc.nb_power_cycles_duration_with_power_state" : ["main.adhoc.nb_power_cycles_duration_ALL", "main.adhoc.nb_power_modes_events_duration_ALL"],
    "main.adhoc.nb_power_cycles_duration_with_power_state_vin" : ["main.adhoc.nb_power_cycles_duration_with_power_state", "main.rel_fleet_metrics.vehicles_metadata"],
    "main.adhoc.nb_btm_nfc_sensor_health" : ["low_fidelity_data.denormalized_obj"],
    "main.adhoc.nb_btm_nfc_sensor_health_temp" : ["main.adhoc.nb_btm_nfc_sensor_health"],
    "main.adhoc.nb_btm_nfc_sensor_health_with_vin_power_cycles" : ["main.adhoc.nb_power_cycles_duration_with_power_state_vin", "main.adhoc.nb_btm_nfc_sensor_health_temp"],
    "main.adhoc.nb_btm_nfc_offline_with_vin_power_cycles" : ["main.adhoc.nb_btm_nfc_sensor_health_with_vin_power_cycles"],
    "main.adhoc.nb_btm_nfc_offline_daily_with_vin" : ["main.adhoc.nb_btm_nfc_sensor_health_with_vin_power_cycles"],
    "main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames": ["main.adhoc.nb_btm_nfc_offline_can_frames", "main.adhoc.nb_btm_nfc_offline_daily_with_vin"],
    "main.adhoc.nb_BTM_can_errors_log_count": ["high_fidelity_customer_vehicles_data.parsed_pcap_message_customer_vehicles_v2"],
    "main.adhoc.nb_BTM_can_errors_with_vin_power_cycles" : ["main.adhoc.nb_power_cycles_duration_with_power_state_vin", "main.adhoc.nb_BTM_can_errors_log_count"],
    "main.adhoc.nb_BTM_can_errors_daily_with_vin" : ["main.adhoc.nb_BTM_can_errors_with_vin_power_cycles"],
    "main.adhoc.nb_BTM_can_errors_daily_module_status_transition" : ["main.adhoc.nb_btm_nfc_offline_CAN_frames_with_errors_daily"],
    "main.adhoc.nb_btm_nfc_sensor_CAN_frames_lowfi" : ["low_fidelity_data.denormalized_obj"],
    "main.adhoc.nb_btm_nfc_CAN_frames_lowfi_flag" : ["main.adhoc.nb_btm_nfc_sensor_CAN_frames_lowfi"],
    "main.adhoc.nb_btm_nfc_health_power_cycles_CAN_frames": ["main.adhoc.nb_power_cycles_duration_with_power_state_vin", "main.adhoc.nb_btm_nfc_CAN_frames_lowfi_flag"],
    "main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames_SECOND" : ["main.adhoc.nb_btm_nfc_health_power_cycles_CAN_frames", "main.adhoc.nb_btm_nfc_offline_daily_with_vin"],
    "main.adhoc.nb_btm_nfc_offline_CAN_frames_with_errors_daily": ["main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames_SECOND", "main.adhoc.nb_BTM_can_errors_daily_with_vin"]
}

# Creating a directed graph
G = nx.DiGraph()

# Adding nodes and edges to the graph
for node, ancestors in dependency_graph.items():
    for ancestor in ancestors:
        G.add_edge(ancestor, node)

# Define the node of interest
target_node = "main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames_SECOND"

# Compute all ancestors of the target node
ancestors = nx.ancestors(G, target_node)
ancestors.add(target_node)

# Subgraph containing only the ancestors of the target node
subgraph = G.subgraph(ancestors)

# Plotting the graph
plt.figure(figsize=(15, 10))
pos = nx.drawing.nx_agraph.graphviz_layout(subgraph, prog='dot')
nx.draw(subgraph, pos, with_labels=True, arrows=True)
plt.title("Ancestor Graph of " + target_node)
plt.show()


# COMMAND ----------


