# Databricks notebook source
# MAGIC %md
# MAGIC # Resilient Query Runner

# COMMAND ----------

# MAGIC %md
# MAGIC ## Imports

# COMMAND ----------

# MAGIC %md
# MAGIC ### Module Imports

# COMMAND ----------

# basic imports
import json

# alias imports
import pandas as pd
from typing import Tuple
from datetime import datetime, timedelta
from pyspark.sql import SparkSession
from pyspark.sql.functions import max as spark_max

# COMMAND ----------

# MAGIC %md
# MAGIC ### File Imports

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC ## File Dependencies

# COMMAND ----------

with open("./query_list.json", "r") as f:
  queries = json.load(f)
