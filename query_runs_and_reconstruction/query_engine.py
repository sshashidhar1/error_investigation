from datetime import datetime, timedelta
from pyspark.sql import SparkSession
from typing import List, Tuple
import logging

# Setting up logging configuration
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s:%(message)s')

class QueryEngine:
    """
    A class to handle the execution of SQL queries using Apache Spark.

    Attributes:
        table (str): Name of the target table in the database.
        dates (List[Tuple[str, str]]): List of date range tuples in 'YYYY-MM-DD' format.
        method (str): Method to handle writing data to table ('overwrite', 'append', etc).
        query (str): The SQL query string to be executed.
        segment_length (int): Length of the date segments for resegmenting date ranges.
        dedupe (bool): Flag to determine whether deduplication is required.
    """

    def __init__(self, table: str, dates: List[Tuple[str, str]], query: str, method: str = None,  segment_length: int = None, dedupe: bool = None):
        """
        The constructor for QueryEngine class.

        Parameters:
            table (str): The name of the table to write results to.
            dates (List[Tuple[str, str]]): Date ranges for the query execution.
            method (str): Method of writing results to the table (default 'append').
            query (str): The query to be executed.
            segment_length (int, optional): The segment length to break down date ranges (default 7).
            dedupe (bool, optional): Flag to indicate if deduplication is needed (default False).
        """
        self.table = table
        self.dates = dates
        self.query = query
        self.method = method if method else "append"
        self.segment_length = segment_length if segment_length else 7
        self.dedupe = dedupe if dedupe else False
        self.date_range = None
        self.dataframe = None
        self.query_to_run = self.query
        logging.info("QueryEngine instance created with table: %s, method: %s", table, self.method)
    
    def resegment_date_ranges(self):
        """
        Segments the provided date ranges into smaller segments.

        This method modifies the instance's date_range attribute with a list of smaller date segments.
        """
        logging.info("Resegmenting date ranges into smaller segments of length %d", self.segment_length)
        if self.dates is None:
            return
        segmented_dates: List[Tuple[str, str]] = []
        for start_str, end_str in self.dates:
            start_date: datetime.date = datetime.strptime(start_str, "%Y-%m-%d").date()
            end_date: datetime.date = datetime.strptime(end_str, "%Y-%m-%d").date()
            
            while start_date < end_date:
                segment_end_date: datetime.date = min(start_date + timedelta(days=self.segment_length), end_date)
                segmented_dates.append((start_date.strftime("%Y-%m-%d"), segment_end_date.strftime("%Y-%m-%d")))
                
                start_date = segment_end_date + timedelta(days=1)
        
        self.date_range = segmented_dates
        self.date_range.reverse()
        logging.info("Date ranges resegmented into %d segments", len(segmented_dates))

    def run_query(self):
        """
        Executes the SQL query using SparkSession.

        Returns:
            A DataFrame containing the results of the query.
        """
        logging.info("Executing query")
        spark = SparkSession.builder.appName("query_runner").getOrCreate()
        spark.conf.set("spark.sql.shuffle.partitions", "auto")
        return spark.sql(self.query_to_run)

    def write_to_table(self, method: str = None):
        """
        Writes the DataFrame to the specified table in the database.

        Parameters:
            method (str, optional): The write method to be used (default is the instance's method).
        """
        if not method:
            logging.info("Writing DataFrame to table %s using default method %s", self.table, self.method)
            self.dataframe.write.option("mergeSchema", "true").mode(self.method).saveAsTable(self.table)
        else:
            logging.info("Writing DataFrame to table %s using method %s", self.table, method)
            self.dataframe.write.option("mergeSchema", "true").mode(method).saveAsTable(self.table)
            logging.info("Wrote DataFrame to table %s using method %s", self.table, method)


    def execute_query(self):
        """
        Manages the execution of the query, including date range segmentation and writing to the table.

        This method formats the query with the specified date ranges, executes it, and writes the results to the table.
        """
        logging.info("Starting query execution")
        if self.dates is None:
            logging.info("Execution with no dates.")
            self.dataframe = self.run_query()
            self.write_to_table(method = 'overwrite')
        elif not self.date_range:
            logging.info("No date range segmentation required, executing for the entire date range")
            self.query = self.query.format(min_date = self.dates[0], max_date = self.dates[1])
            self.dataframe = self.run_query()
            self.write_to_table()
        else:
            logging.info("Executing segmented queries")
            for dates in self.date_range:
                logging.info(f"Executing for {dates[0]} - {dates[1]}")
                self.query_to_run = self.query.format(min_date = dates[0], max_date = dates[1])
                self.dataframe = self.run_query()
                self.write_to_table(method="append")
                logging.info(f"Finished Execution for {dates[0]} - {dates[1]}")
        logging.info("Query execution completed")
