# Databricks notebook source
from pyspark.sql import SparkSession
spark = SparkSession.builder.appName("Test").getOrCreate()

# COMMAND ----------

sql = f"SELECT * FROM main.adhoc.ss_x_btm_nfc_offline_daily_temp_and_can_frames"

# COMMAND ----------

table = spark.sql(sql)

# COMMAND ----------

pdx = table.toPandas()

# COMMAND ----------

pdx.head()

# COMMAND ----------

pdx.columns

# COMMAND ----------

import plotly.express as px

# COMMAND ----------

for col in ['RFD_offline_flag', 'LFD_offline_flag', 'RF_offline_flag', 'FF_offline_flag', 'IC_offline_flag', 'BTM_RFD_offline_temp', 'BTM_LFD_offline_temp', 'BTM_RF_offline_temp', 'BTM_FF_offline_temp', 'BTM_IC_offline_temp', 'NFC_FD_offline', 'NFC_ID_offline']:
  summed_data = pdx.groupby('event_date')[col].sum().reset_index()
  fig = px.line(summed_data, x='event_date', y=col, title=f'Sum of {col} per Day')
  fig.show()  

# COMMAND ----------

for col in ['RFD_offline_flag', 'LFD_offline_flag', 'RF_offline_flag', 'FF_offline_flag', 'IC_offline_flag', 'BTM_RFD_offline_temp', 'BTM_LFD_offline_temp', 'BTM_RF_offline_temp', 'BTM_FF_offline_temp', 'BTM_IC_offline_temp', 'NFC_FD_offline', 'NFC_ID_offline']:
  summed_data = pdx.groupby('event_date')[col].sum().reset_index()
  fig = px.bar(summed_data, x='event_date', y=col, title=f'Sum of {col} per Day')
  fig.show()  

# COMMAND ----------


