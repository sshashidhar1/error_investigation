# Databricks notebook source
import json

# file imports
from query_engine import QueryEngine

# COMMAND ----------

# load the queries
with open("./query_list.json", "r") as f:
  queries = json.load(f)

# COMMAND ----------

table = "main.adhoc.nb_btm_nfc_sensor_health"
dates = [("2023-10-20", "2023-11-12")]
method = "append"
query = """
SELECT _source,
       _updated                                            AS timestamp,
       CAST(Concat_ws('-', YEAR, MONTH, DAY) AS TIMESTAMP) AS date,
       o.`124554772487_2`                                 AS btm_lfd_diagnosis_status_temperature,
       o.`124554903559_2`                                 AS btm_rfd_diagnosis_status_temperature,
       o.`124554641415_2`                                 AS btm_ic_diagnosis_status_temperature,
       o.`124555034631_2`                                 AS btm_rf_diagnosis_status_temperature,
       o.`124554510343_2`                                 AS btm_ff_diagnosis_status_temperature,
       o.`137439281156`                                   AS nfc_fd_health_temperature,
       o.`137439346692`                                   AS nfc_id_health_temperature
FROM   main.low_fidelity_data.denormalized_obj o
WHERE  ( COALESCE(o.`124554772487_2`,  o.`124554903559_2`,  o.`124554641415_2`, 
                  o.`124555034631_2`,  o.`124554510343_2`,  o.`137439281156`, 
                  o.`137439346692`) IS NOT NULL )
       AND DATE(_updated) >= '{min_date}'
       AND DATE(_updated) <= '{max_date}'

"""

# COMMAND ----------

sensor_health = QueryEngine(
  table = table,
  dates = dates,
  method = method,
  query = query
)

# COMMAND ----------

sensor_health.resegment_date_ranges()

# COMMAND ----------

sensor_health.date_range

# COMMAND ----------

sensor_health.execute_query()

# COMMAND ----------

print(sensor_health.dataframe.head(5))

# COMMAND ----------

table = "main.adhoc.nb_btm_nfc_sensor_health_temp"
dates = [("2023-10-20", "2023-11-12")]
method = "append"
query = queries[table]

# COMMAND ----------

sensor_health_temp = QueryEngine(
  table = table,
  dates = dates,
  method = method,
  query = query
)

# COMMAND ----------

sensor_health_temp.resegment_date_ranges()

# COMMAND ----------

sensor_health_temp.execute_query()

# COMMAND ----------

table = "main.adhoc.nb_btm_nfc_sensor_health_with_vin_power_cycles"
dates = [("2023-10-20", "2023-11-08")]
method = "append"
query = queries[table]

# COMMAND ----------

sensor_health_with_vin = QueryEngine(
  table = table,
  dates = dates,
  method = method,
  query = query
)

# COMMAND ----------

query

# COMMAND ----------

sensor_health_with_vin.resegment_date_ranges()

# COMMAND ----------

sensor_health_with_vin.execute_query()

# COMMAND ----------

table = "main.adhoc.nb_btm_nfc_sensor_health_with_vin_power_cycles"
dates = [("2023-10-20", "2023-10-25")]
method = "append"
query = queries[table]

# COMMAND ----------

x = QueryEngine(
  table = table,
  dates = dates,
  method = method,
  query = query
)

# COMMAND ----------

x.resegment_date_ranges()

# COMMAND ----------

x.execute_query()

# COMMAND ----------

table = "main.adhoc.nb_btm_nfc_offline_daily_with_vin"
dates = [("2023-10-20", "2023-10-25")]
method = "append"
query = queries[table]

# COMMAND ----------

x = QueryEngine(
  table = table,
  dates = dates,
  method = method,
  query = query
)

# COMMAND ----------

x.resegment_date_ranges()

# COMMAND ----------

x.execute_query()

# COMMAND ----------


