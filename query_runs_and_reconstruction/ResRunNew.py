# Databricks notebook source
# MAGIC %md
# MAGIC # Resilent Runner

# COMMAND ----------

# module imports
import json
import pandas as pd
from typing import Tuple
from datetime import datetime, timedelta
from pyspark.sql import SparkSession
from pyspark.sql.functions import max as spark_max

# file imports
from query_engine import QueryEngine

# COMMAND ----------

import time

# COMMAND ----------

def replace_nb_with_ssx(input_string):
    # Replace all occurrences of 'nb' with 'ssy'
    return input_string.replace("nb", "ss_higfid")

# COMMAND ----------

# load the queries
with open("./query_list.json", "r") as f:
  queries = json.load(f)

# COMMAND ----------

def staterun(q):
  q.resegment_date_ranges()
  q.execute_query()
  del q
  return

# COMMAND ----------

spark_session = SparkSession.builder.appName("spark").getOrCreate()

# COMMAND ----------

dates_ranges = [("2023-06-01", "2023-06-02")]

# COMMAND ----------

spark = SparkSession.builder.appName("Dedupe").getOrCreate()

# COMMAND ----------

def deduplicate_and_write_table(table_name):
    # Initialize Spark session
    global spark

    # Read the table
    df = spark.table(table_name)

    # Drop duplicate rows
    df_deduplicated = df.dropDuplicates()

    # Overwrite the original table with the deduplicated DataFrame
    df_deduplicated.write.format("delta").mode("overwrite").saveAsTable(table_name)

# COMMAND ----------

TABLE_RANGE = [
  # "main.adhoc.nb_power_modes_events",
  # "main.adhoc.nb_power_modes_events_duration",
  # "main.adhoc.nb_power_modes_events_duration_ALL",
  # "main.adhoc.nb_power_cycles_duration",
  # "main.adhoc.nb_power_cycles_duration_ALL",
  # "main.adhoc.nb_power_cycles_duration_with_power_state",
  # "main.adhoc.nb_power_cycles_duration_with_power_state_vin",
  # "main.adhoc.nb_btm_nfc_sensor_health",
  # "main.adhoc.nb_btm_nfc_sensor_health_temp",
  # "main.adhoc.nb_btm_nfc_sensor_health_with_vin_power_cycles",
  # "main.adhoc.nb_btm_nfc_offline_with_vin_power_cycles",
  # "main.adhoc.nb_btm_nfc_offline_daily_with_vin",
  "main.adhoc.nb_BTM_can_errors_log_count",
  "main.adhoc.nb_BTM_can_errors_with_vin_power_cycles",
  "main.adhoc.nb_BTM_can_errors_daily_with_vin",
  "main.adhoc.nb_btm_nfc_sensor_CAN_frames_lowfi",
  "main.adhoc.nb_btm_nfc_CAN_frames_lowfi_flag",
  "main.adhoc.nb_btm_nfc_health_power_cycles_CAN_frames",
  "main.adhoc.nb_btm_nfc_offline_daily_temp_and_can_frames"
]

# COMMAND ----------

for table in TABLE_RANGE:
  start = time.time()
  print(f"Table: {table}")
  if table == "main.adhoc.nb_BTM_can_errors_log_count":
    # execute the different query logic, from high fidelity for these days
    query = """
      select *, 
            (BTM_RFD_CAN_Error_Logging_Cnt + BTM_LFD_CAN_Error_Logging_Cnt + BTM_RF_CAN_Error_Logging_Cnt + BTM_FF_CAN_Error_Logging_Cnt + BTM_IC_CAN_Error_Logging_Cnt) as BTM_total_CAN_Error_logging_Cnt
      from
      (
      select id as car_ownership_id, timestamp, date,
            case when signal.name = 'BTM_RFD_CAN_Error_Logging_Cnt' then signal.value else 0 end as BTM_RFD_CAN_Error_Logging_Cnt,
            case when signal.name = 'BTM_LFD_CAN_Error_Logging_Cnt' then signal.value else 0 end as BTM_LFD_CAN_Error_Logging_Cnt,
            case when signal.name = 'BTM_RF_CAN_Error_Logging_Cnt' then signal.value else 0 end as BTM_RF_CAN_Error_Logging_Cnt,
            case when signal.name = 'BTM_FF_CAN_Error_Logging_Cnt' then signal.value else 0 end as BTM_FF_CAN_Error_Logging_Cnt,
            case when signal.name = 'BTM_IC_CAN_Error_Logging_Cnt' then signal.value else 0 end as BTM_IC_CAN_Error_Logging_Cnt
      from main.high_fidelity_rivian_vehicles_data.parsed_pcap_message_rivian_vehicles_v2
      where signal.name in ('BTM_LFD_CAN_Error_Logging_Cnt', 'BTM_RFD_CAN_Error_Logging_Cnt', 'BTM_FF_CAN_Error_Logging_Cnt', 
                            'BTM_RF_CAN_Error_Logging_Cnt', 'BTM_IC_CAN_Error_Logging_Cnt')
            AND date >= '{min_date}' AND date<= '{max_date}'
            AND signal.value !=0
      )
    """
    query = replace_nb_with_ssx(query)
    table = replace_nb_with_ssx(table)
    stateful = QueryEngine(
      table = table,
      dates = dates_ranges,
      query = query
    )
    staterun(stateful)
    end = time.time()
    print(f"Table: {table}. Total Time {end - start}")
    deduplicate_and_write_table(table)
    continue
  query = replace_nb_with_ssx(queries[table])
  table = replace_nb_with_ssx(table)
  stateful = QueryEngine(
    table = table,
    dates = dates_ranges,
    query = query
  )
  staterun(stateful)
  end = time.time()
  print(f"Table: {table}. Total Time {end - start}")


# COMMAND ----------


